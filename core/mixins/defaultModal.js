export default {
  methods: {
    defaultModalShow(component, params = {}) {
      this.$modal.hide('defaultModal');
      this.$modal.show('defaultModal', { component, ...params });
    },

    defaultModalHide() {
      this.$modal.hide('defaultModal');
    },
  },
};
