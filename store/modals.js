export const state = () => ({
  openModals: [],
});

export const getters = {
  getModalsState(state) {
    return !!state.openModals.length;
  },

  getModalsList(state) {
    return state.openModals;
  },
};

export const mutations = {
  add(state, data) {
    if (!state.openModals.includes(data)) {
      state.openModals.push(data);
    }
  },

  remove(state, data) {
    const index = state.openModals.findIndex(item => item === data);

    if (index !== -1) {
      state.openModals.splice(index, 1);
    }
  },

  clear(state) {
    state.openModals = [];
  },
};
