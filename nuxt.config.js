export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.npm_package_name || '',
    htmlAttrs: {
      lang: 'ru',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=360, initial-scale=1, user-scalable=no, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { name: 'msapplication-TileColor', content: '#133d5f' },
      { name: 'theme-color', content: '#133d5f' },
      { name: 'msapplication-config', content: 'favicons/browserconfig.xml' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicons/favicon.ico' },
      { rel: 'shortcut icon', href: '/favicons/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicons/favicon-16x16.png' },
      { rel: 'icon', sizes: '192x192', href: '/favicons/android-chrome-192x192.png' },
      { rel: 'icon', sizes: '512x512', href: '/favicons/android-chrome-512x512.png' },
      { rel: 'manifest', href: '/favicons/site.webmanifest' },
      { rel: 'mask-icon', href: '/favicons/safari-pinned-tab.svg', color: '#133d5f' },
    ],
  },

  loading: { color: '#1f6d39' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/app.scss'],

  styleResources: {
    scss: ['assets/scss/vars/scss.scss', 'assets/scss/mixins.scss'],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~plugins/vue-js-modal.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios', '@nuxtjs/style-resources', '@nuxtjs/svg-sprite', '@nuxtjs/auth-next'],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://api.hydrapay.ru/',
  },

  auth: {
    strategies: {
      laravelJWT: {
        provider: 'laravel/jwt',
        url: '/',
        endpoints: {
          login: { url: 'auth/login', method: 'post', propertyName: false },
          logout: { url: 'auth/logout', method: 'post', propertyName: false },
          refresh: { url: 'auth/refresh', method: 'post', propertyName: false },
          user: { url: 'auth/user', method: 'get', propertyName: false },
        },
        token: {
          property: 'access_token',
          maxAge: 10,
        },
        refreshToken: {
          maxAge: 20160 * 60,
        },
      },
    },
  },

  /*  auth: {
    strategies: {
      laravelPassport: {
        provider: 'laravel/passport',
        endpoints: {
          userInfo: '...',
        },
        url: '...',
        clientId: '...',
        clientSecret: '...',
      },
    },
  }, */

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    parallel: true,
  },
};
